// Game.tsx
import React, { useState, useEffect } from 'react';
import Card from './Card';

const shapes = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];

const Game: React.FC = () => {
  const [cards, setCards] = useState<{ shape: string; color: string; id: number }[]>([]);
  const [flipped, setFlipped] = useState<number[]>([]);
  const [matchedPairs, setMatchedPairs] = useState<number[]>([]);
  const [attempts, setAttempts] = useState<number>(0);

  useEffect(() => {
    // Initialize the cards array with random pairs
    const shuffledShapes = [...shapes, ...shapes].sort(() => Math.random() - 0.5);
    const initialCards = shuffledShapes.map((shape, index) => ({
      shape,
      color: getRandomColor(),
      id: index,
    }));
    setCards(initialCards);
  }, []);

  const handleCardClick = (index: number) => {
    if (flipped.length < 2 && !flipped.includes(index)) {
      setFlipped([...flipped, index]);
    }

    if (flipped.length === 1) {
      setAttempts(attempts + 1);

      if (cards[flipped[0]].shape === cards[index].shape) {
        setMatchedPairs([...matchedPairs, cards[flipped[0]].id, cards[index].id]);
        setFlipped([]);
      } else {
        setTimeout(() => {
          setFlipped([]);
        }, 1000);
      }
    }
  };

  const getRandomColor = () => {
    const colors = ['red', 'green', 'blue'];
    return colors[Math.floor(Math.random() * colors.length)];
  };

  return (
    <div className="game">
      <div className="grid">
        {cards.map((card, index) => (
          <Card
            key={index}
            shape={card.shape}
            color={card.color}
            onClick={() => handleCardClick(index)}
            isFlipped={flipped.includes(index) || matchedPairs.includes(card.id)}
          />
        ))}
      </div>
      {matchedPairs.length === cards.length && <div className="message">Congratulations! You completed the game in {attempts} attempts.</div>}
    </div>
  );
};

export default Game;
