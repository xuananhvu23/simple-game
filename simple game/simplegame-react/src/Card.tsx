// Card.tsx
import React, { useState } from 'react';

interface CardProps {
  shape: string;
  color: string;
  onClick: () => void;
  isFlipped: boolean;
}

const Card: React.FC<CardProps> = ({ shape, color, onClick, isFlipped }) => {
  return (
    <div
      className={`card ${isFlipped ? 'flipped' : ''}`}
      onClick={onClick}
      style={{ backgroundColor: color }}
    >
      {isFlipped ? shape : null}
    </div>
  );
};

export default Card;
