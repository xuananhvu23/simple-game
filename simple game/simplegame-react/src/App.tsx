import React, { useState, useEffect } from 'react';
import './App.css';

const shapes = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];

const generateCards = () => {
  const shuffledShapes = [...shapes, ...shapes].sort(() => Math.random() - 0.5);
  return shuffledShapes.map((shape, index) => ({
    id: index,
    shape,
    isFlipped: false,
  }));
};

const App: React.FC = () => {
  const [cards, setCards] = useState(generateCards());
  const [flippedCount, setFlippedCount] = useState(0);
  const [firstFlippedCard, setFirstFlippedCard] = useState<number | null>(null);
  const [attempts, setAttempts] = useState(0);

  useEffect(() => {
    if (flippedCount === 2) {
      const [card1, card2] = cards.filter((card) => card.isFlipped);
      if (card1.shape === card2.shape) {
        setFlippedCount(0);
      } else {
        setTimeout(() => {
          setCards((prevCards) =>
            prevCards.map((card) =>
              card.isFlipped ? { ...card, isFlipped: false } : card
            )
          );
          setFlippedCount(0);
        }, 1000);
      }
      setAttempts((prevAttempts) => prevAttempts + 1);
    }
  }, [cards, flippedCount]);

  const handleCardClick = (index: number) => {
    if (flippedCount === 0) {
      setFirstFlippedCard(index);
    }

    setCards((prevCards) =>
      prevCards.map((card, cardIndex) =>
        cardIndex === index
          ? { ...card, isFlipped: true }
          : cardIndex === firstFlippedCard
          ? { ...card, isFlipped: true }
          : card
      )
    );

    setFlippedCount((prevFlippedCount) => prevFlippedCount + 1);
  };

  const isGameFinished = cards.every((card) => card.isFlipped);

  return (
    <div className="App">
      <h1>Memory Game</h1>
      <div className="Grid">
        {cards.map((card, index) => (
          <div
            key={index}
            className={`Card ${card.isFlipped ? 'Flipped' : ''}`}
            onClick={() => handleCardClick(index)}
          >
            {card.isFlipped ? card.shape : ''}
          </div>
        ))}
      </div>
      {isGameFinished && (
        <div className="Message">
          Congratulations! You completed the game in {attempts} attempts.
        </div>
      )}
    </div>
  );
};

export default App;
